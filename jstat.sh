#!/bin/bash

#############################################################################
# Audit memoire jvm
#	-1 creation db audit 
#	-2 creation table via fichier sql.txt
#	-3 crontab "*/5 * * * *  /home/username/scripts/jstat.sh"
#	-4 ajouter fichier php dans un apache
#############################################################################

ENV=$(echo E$(hostname | cut -c4-5) | tr "[A-Z]" "[a-z]")
DATE=$(date +"%Y-%m-%d %H:%M:%S")

for i in $(ls /var/run/${ENV}*.pid )
do
        set -- $(jstat -gccapacity $(cat $i) |  tail -1)
        echo "insert into memory_jvm.jstat_gccapacity values ('$(basename $i | sed 's/.pid//g')' ,TIMESTAMP '$DATE', $(echo $* | sed 's/,/./g' | sed 's/ /,/g') );"

        set -- $(jstat -gc $(cat $i) |  tail -1)
        echo "insert into memory_jvm.jstat_gc values ('$(basename $i | sed 's/.pid//g')' ,TIMESTAMP '$DATE', $(echo $* | sed 's/,/./g' | sed 's/ /,/g') );"
done

