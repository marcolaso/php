<?php

$liste_instance=array();
$pgCnx = pg_connect('host=localhost dbname=audit user=toto password=toto') or die('Pb lors de la connxion PG');

echo "<html>";

if( isset($_POST[int_date])) { 
  $env=$_POST[env_name];
  $date=$_POST[int_date];
 
  echo "
    <head>
      <script type='text/javascript' src='https://www.gstatic.com/charts/loader.js'></script>
      <script type='text/javascript'>
        google.charts.load('current', {'packages':['corechart']});
        google.charts.setOnLoadCallback(drawChart);

        function drawChart() {
          var data;
          var options;
          var date;
          var chart;\n\n";

#boucle sur toutes les instances !
          $sql = "SELECT distinct(instance) FROM memory_jvm.jstat_gc WHERE instance like '$env%' ORDER BY instance;";
          $pgQuery = pg_query($pgCnx,$sql) or die('Erreur execution de la requête');
 
          while ($row = pg_fetch_array($pgQuery)) {
             $liste_instance[]=$row[0];
          }
  
          foreach ($liste_instance as $instance) {
            $tableau='';
          
            echo "data = google.visualization.arrayToDataTable([['date','ccsc','ccsmn','ccsmx','ccsu','jstat_gc.cgc','cgct','jstat_gc.ec','eu','jstat_gc.fgc','fgct','gct','jstat_gc.mc','mcmn','mcmx','mu','ngc','ngcmn','ngcmx','jstat_gc.oc','ogc','ogcmn','ogcmx','ou','jstat_gc.s0c','s0u','jstat_gc.s1c','s1u','jstat_gc.ygc' ],";
            #echo "data = google.visualization.arrayToDataTable([['date','ccsc','ccsmn','ccsmx','ccsu','jstat_gc.cgc','cgct','jstat_gc.ec','eu',',jstat_gc.fgc' ,'fgct'],";

# recup data

            #$sql = "SELECT jstat_gc.date,jstat_gc.ccsc ,ccsmn ,ccsmx ,ccsu ,jstat_gc.cgc ,cgct ,jstat_gc.ec ,eu ,jstat_gc.fgc ,fgct ,gct ,jstat_gc.mc ,mcmn ,mcmx ,mu ,ngc ,ngcmn ,ngcmx ,jstat_gc.oc ,ogc ,ogcmn ,ogcmx ,ou ,jstat_gc.s0c ,s0u ,jstat_gc.s1c ,s1u ,jstat_gc.ygc  
            $sql = "SELECT jstat_gc.date,jstat_gc.ccsc ,ccsmn ,ccsmx ,ccsu ,jstat_gc.cgc ,cgct ,jstat_gc.ec ,eu ,jstat_gc.fgc,fgct,gct ,jstat_gc.mc ,mcmn ,mcmx ,mu ,ngc ,ngcmn ,ngcmx ,jstat_gc.oc ,ogc ,ogcmn ,ogcmx ,ou ,jstat_gc.s0c ,s0u ,jstat_gc.s1c ,s1u ,jstat_gc.ygc
                    FROM memory_jvm.jstat_gccapacity,memory_jvm.jstat_gc 
                    WHERE jstat_gccapacity.instance = jstat_gc.instance 
                    AND jstat_gccapacity.date = jstat_gc.date 
                    AND jstat_gc.instance = '$instance'
                    AND jstat_gc.date between '$date 00:00:01' and '$date 23:59:59' ;";


            $pgQuery = pg_query($pgCnx,$sql) or die('Erreur execution de la requête');
   
            while ($row = pg_fetch_array($pgQuery)) {
               $tableau.="['".$row[0]."',".$row[1].",".$row[2].",".$row[3].",".$row[4].",".$row[5].",".$row[6].",".$row[7].",".$row[8].",".$row[9].",".$row[10].",".$row[11].",".$row[12].",".$row[13].",".$row[14].",".$row[15].",".$row[16].",".$row[17].",".$row[18].",".$row[19].",".$row[20].",".$row[21].",".$row[22].",".$row[23].",".$row[24].",".$row[25].",".$row[26].",".$row[27].",".$row[28]."],\n";
            }
   
            $tableau=substr($tableau,0,strlen($tableau)-2);
            echo $tableau  ;

            echo "]);

            options = {
              title: '$env - Instance ".substr($instance, 3, strlen($instance))."',
              curveType: 'function',
              legend: { position: 'bottom' }
            };

            chart = new google.visualization.LineChart(document.getElementById('$instance'));

            chart.draw(data, options);\n ";
          }
        echo "}
      </script>
    </head>";
}
echo '<body>';

$sql="SELECT DISTINCT(to_char(date, 'yyyy/mm/dd')) 
      FROM memory_jvm.jstat_gc 
      WHERE date between  now() - justify_days(interval '15 days')  and now() 
      ORDER BY to_char(date, 'yyyy/mm/dd') DESC;";

$pgQuery = pg_query($pgCnx,$sql) or die('Erreur execution de la requête date');
while ($row = pg_fetch_array($pgQuery)) {
      $date.='<option value="'.$row[0].'">'.$row[0].'</option>\n';
}

echo '<form method="post" action="/jstat.php">
            <select name="int_date" id="int_date">
                  '.$date.'
            </select>
            <select name="env_name" id="env_name">
               <option value="epr">EPR</option>
               <option value="edw">EDW</option>
            </select>
            <button type="submit">new graph</button>
         </form>';
if( isset($_POST[int_date])) { 
    echo " Graphique pour l'environement $env à la date du $_POST[int_date]";
    foreach ($liste_instance as $instance) {
      echo "<div id='$instance' style='width: 2000px; height: 500px'></div>\n";
    }
}
echo " </body>
</html>";

?>
